/*
 * Freq_calc_phsr_ang.h
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_FREQ_CALC_PHSR_ANG_H_
#define INCLUDE_FREQ_CALC_PHSR_ANG_H_

#include "common_defines.h"

float Freq_calc_phsr_ang(float sig_in[], uint8_t grid_frequency);   // function definition - (sig_in = angle_value of positive sequence RADIAN)


#endif /* INCLUDE_FREQ_CALC_PHSR_ANG_H_ */
