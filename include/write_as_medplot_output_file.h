/*
 * write_as_medplot_output_file.h
 *
 *  Created on: 27 de abr de 2019
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_WRITE_AS_MEDPLOT_OUTPUT_FILES_H_
#define INCLUDE_WRITE_AS_MEDPLOT_OUTPUT_FILES_H_

#include <complex.h>
#include "common_defines.h"

void write_as_medplot_output_file(float timestamp, struct phasor PhasorVA, struct phasor PhasorVB, struct phasor PhasorVC, float frequency, float ROCOF);

#endif /* INCLUDE_WRITE_AS_MEDPLOT_OUTPUT_FILES_H_ */
