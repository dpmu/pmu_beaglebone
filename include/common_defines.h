/*
 * common_defines.h
 *
 *  Created on: 5 de abr de 2020
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_COMMON_DEFINES_H_
#define INCLUDE_COMMON_DEFINES_H_

#include <stdint.h>
#include <complex.h>


#define C_D_PMU_OPPERATION_MODE 1 //0 = read stimulus from a file, 1 = read stimulus from (PRU loader/ADC samples)

//Output file place with estimated phasors
#define C_OUTPUTS_FILENAME "/tmp/Beaglebone_processed_outputs.txt"

//Number of points per cycle
#define C_SAMPLES_PER_CYCLE 64

//Timebase for frac sec incrementation.
#define C_PMU_CFG_TIME_BASE 600 //This field must be changed with FRACSEC field together

//BOOL variable definition
#define bool uint8_t
#define true 1
#define false 0

//Phasor struct
struct phasor{
	//phasor declaration
	float X_real;
	float X_imag;
};

//Meter values
struct meter_values{
    uint16_t cont_amos[C_SAMPLES_PER_CYCLE];
    float time[C_SAMPLES_PER_CYCLE];
    float tensao_fa[C_SAMPLES_PER_CYCLE];
    float corre_fa[C_SAMPLES_PER_CYCLE];
    float tensao_fb[C_SAMPLES_PER_CYCLE];
    float corre_fb[C_SAMPLES_PER_CYCLE];
    float tensao_fc[C_SAMPLES_PER_CYCLE];
    float corre_fc[C_SAMPLES_PER_CYCLE];
    unsigned long int file_pointer; //File pointer returned AFTER the read access
    bool reduced_pts_cycle; //flag that identify the necessity to iterpolation.
    uint8_t last_iter_val; // pointer identifying the last CYCLE poit read (usually C_SAMPLES_PER_PERIOD), but when an eof is arrested, it will have the last data readed.
    uint32_t soc[C_SAMPLES_PER_CYCLE];
};

//Seq Components struct - used on Seq_componts scripts
struct SEQ_COMPONETS{
	//Components seq declaration
	float complex Pos_seq;
	float complex Neg_seq;
	float complex Zer_seq;
};

//Path to FIFO - HANDLER2PMU //Removed from Samples handler
#define C_FIFO_PATH "/tmp/handler2pmu/"
#define C_FIFO_PATHNAME "/tmp/handler2pmu/h2pfifo"  /* Path used on ftok for shmget key  */

//Pi value constant
#define C_PI 3.1415926535897932384626433832795 //more resolution
#define C_TWOPI 6.283185307179586476925286766559 //more resolution


#endif /* INCLUDE_COMMON_DEFINES_H_ */
