/*
 * adjust_phasor_angl.h
 *
 *  Created on: 16 de jul de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_ADJUST_PHASOR_ANGL_H_
#define INCLUDE_ADJUST_PHASOR_ANGL_H_

#include "common_defines.h"

struct phasor adjust_phasor_angl ( struct phasor phasor_in, float MagAdjust  , float angle_adjust);   // function definition


#endif /* INCLUDE_ADJUST_PHASOR_ANGL_H_ */
