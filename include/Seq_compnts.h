/*
 * Seq_compnts.h
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_SEQ_COMPNTS_H_
#define INCLUDE_SEQ_COMPNTS_H_
#include "common_defines.h"

//alfa and alfa^2 values definitions - To components seq calculus - CAREFUL WITH RESOLUTION
#define C_COMP_ALPH   -0.5+I*0.8660254 //Rotation fator
#define C_COMP_ALPH2   -0.5-I*0.8660254

struct SEQ_COMPONETS Seq_compnts(struct phasor Phasor_VA, struct phasor Phasor_VB, struct phasor Phasor_VC);


#endif /* INCLUDE_SEQ_COMPNTS_H_ */
