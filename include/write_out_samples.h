/*
 * write_out_samples.h
 *
 *  Created on: 27 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_WRITE_OUT_SAMPLES_H_
#define INCLUDE_WRITE_OUT_SAMPLES_H_

#include <stdint.h>
#include "common_defines.h"


#define C_OUTPUTS_FILENAME_SAMPLES "/tmp/Beaglebone_processed_samples_out0.txt"

//Functions Prototypes
void write_out_samples(uint16_t cont_amos[256],  float time[256], float tensao_fa[256], float tensao_fb[256], float tensao_fc[256] );


#endif /* INCLUDE_WRITE_OUT_SAMPLES_H_ */
