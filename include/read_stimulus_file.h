/*
 * read_stimulus_file.h
 *
 *  Created on: 21 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_READ_STIMULUS_FILE_H_
#define INCLUDE_READ_STIMULUS_FILE_H_

#define C_VALUE_SCALE 0 // Read values scale -> 0 =V/A; 3=KV/KA; 6=MV/MA
#define C_SER_PMU_STIM_FILE0 "/tmp/serial_pmu_stimulus.txt"


struct meter_values read_stimulus_file( int header_lines, long int file_l_pointer, uint8_t reserved);   // function definition


#endif /* INCLUDE_READ_STIMULUS_FILE_H_ */
