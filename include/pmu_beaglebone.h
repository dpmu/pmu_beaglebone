/*
 * pmu_beaglebone.h
 *
 *  Created on: 23 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_PMU_BEAGLEBONE_H_
#define INCLUDE_PMU_BEAGLEBONE_H_
	#include <unistd.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdint.h>
	#include <fcntl.h>
	#include <sys/shm.h> //for memory share between the two process
	#include <string.h> //used for memset usage --for fifo to c37118 program
	#include <math.h>
	#include "DFT_phasor.h"
	#include "Seq_compnts.h"
	#include "Freq_calc_phsr_ang.h"
	#include "write_as_medplot_output_file.h"
	#include "write_out_values.h"
	#include "write_out_samples.h" //just for phases samples
	#include "write_out_all_values.h" //Debug and matlab use output
	#include "adjust_phasor_angl.h" //							//ajust with a constante angle and magnitude parcel
	#include "P_Gain_dev_freq.h"							//Adjust with P+gain the phasor
	#include "common_defines.h"


struct PMU_FINAL_DATA{
    //uint16_t FRAMESIZE;
    uint32_t SOC;
    uint32_t FRACSEC;
    uint16_t CONT_IDX, STAT_DATA;
    uint32_t TIME_BASE; //for configuration frame
    uint8_t frame_type;
    float PhsA_real;
    float PhsA_imag;
    float PhsB_real;
    float PhsB_imag;
    float PhsC_real;
    float PhsC_imag;
    float Pos_seq_real;
    float Pos_seq_imag;
    float FREQ, DFREQ;
};

//FIFO pmu2c37118
//Path to FIFO - METER2PMU
#define C_FIFO_PATH_SERVER "/tmp/pmu2C37_118/"
#define C_FIFO_PATHNAME_SERVER "/tmp/pmu2C37_118/pmu2c37"  /* Path used on ftok for shmget key  */
#define SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE (5*4) //Data length from FIFO. 5 float values * 4 bytes




int main(void);
	//int main(int argc, char *argv[] );

#endif /* INCLUDE_PMU_BEAGLEBONE_H_ */
