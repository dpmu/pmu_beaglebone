/*
 * DFT_phasor.h
 *
 *  Created on: 23 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_DFT_PHASOR_H_
#define INCLUDE_DFT_PHASOR_H_

#include <stdint.h>
#include "common_defines.h"

struct phasor DFT_phasor ( float discr_sig[] , int point_dft , uint8_t cycle_points_type);

#endif /* INCLUDE_DFT_PHASOR_H_ */
