/*
 * P_Gain_dev_freq.h
 *
 *  Created on: 18 de Mar de 2019
 *      Author: Maique Garcia
 */


//Gain P compensation
//Author : Maique C Garcia
//Description:
//			This function computes the Gain P parcel based on frequency deviation
//--------------------------------------------------------------------------------
//Requirements:
// Phases phasors
//--------------------------------------------------------------------------------
//Version control
// Vr number     /   Date   / Description
// 0.1           / 18-03-19 / Created initial version

#ifndef INCLUDE_P_GAIN_DEV_FREQ_
#define INCLUDE_P_GAIN_DEV_FREQ_

#include "common_defines.h"
#include <complex.h>
#include <math.h>
#include <stdio.h>

float complex P_Gain_dev_freq(float est_freq, uint8_t sys_frequency);   // function definition

#endif /* INCLUDE_P_GAIN_DEV_FREQ_ */
