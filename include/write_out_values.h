/*
 * write_out_values.h
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_WRITE_OUT_VALUES_H_
#define INCLUDE_WRITE_OUT_VALUES_H_
	#include <complex.h>
	void write_out_values(float timestamp, struct phasor PhasorVA, struct phasor PhasorVB, struct phasor PhasorVC, float frequency, float complex Pos_seq, float complex Neg_seq, float complex Zer_seq );
#endif /* INCLUDE_WRITE_OUT_VALUES_H_ */
