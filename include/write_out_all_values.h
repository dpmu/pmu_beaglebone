/*
 * write_out_all_values.h
 *
 *  Created on: 14 de jun de 2018
 *      Author: Maique Garcia
 */



#ifndef INCLUDE_WRITE_OUT_ALL_VALUES_H_
#define INCLUDE_WRITE_OUT_ALL_VALUES_H_

#include <stdint.h>
#include "common_defines.h"

#define C_BUFFER_CIRCULAR 1

//Functions prototypes
int write_out_all_values(uint16_t timestamp[], float PhaseVA[], float PhaseVB[], float PhaseVC[] ,struct phasor PhasorVA, struct phasor PhasorVB, struct phasor PhasorVC,  float complex Pos_seq, float complex Neg_seq, float complex Zer_seq, float estimated_freq, int saved_counts, FILE *fp);

#endif /* INCLUDE_WRITE_OUT_ALL_VALUES_H_ */
