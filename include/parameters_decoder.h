/*
 * parameters_decoder.h
 *
 *  Created on: 08 de mar de 2020
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_PARAMETERS_DECODER_H_
#define INCLUDE_PARAMETERS_DECODER_H_
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//Public defines
#define TRUE 1
#define FALSE 0

typedef enum
{
	FREQ_OBTAINED_FROM_POS_SEQ_PHASOR = 0,
	FREQ_OBTAINED_FROM_PHASE_A_PHASOR,
	FREQ_OBTAINED_FROM_PHASE_B_PHASOR,
} freq_phasor_estimation_mode_enum;

struct dpmu_file_decoded_st
{
	char *angle_adjust_constant[3]; //3 phases
	uint8_t number_of_phasors; // 3 for  VA, VB, VC; 6 for VA, IA, VB, IB, VC, IC; 7 for VA, IA, VB, IB, VC, IC, PosSeqV
	freq_phasor_estimation_mode_enum phasor_ref_mode;
	uint8_t stimulus_mode;
	uint8_t nominal_sys_freq;
};

//prototype of public functions
void decode_DPMU_file_parameters_pmu(void);
void return_dpmu_angle_adjusts(float *angles_adjust);
uint8_t return_dpmu_stimulus_mode();
uint8_t return_dpmu_number_of_phasors(void);
freq_phasor_estimation_mode_enum return_dpmu_freq_estimation_mode(void);
uint8_t return_dpmu_nominal_freq();

#endif /* INCLUDE_PARAMETERS_DECODER_H_ */
