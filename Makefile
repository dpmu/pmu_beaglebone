PROJECT=pmu_beaglebone


# Two additional CFLAGS must be used for Angstrom
# They must not be used for Debian or Ubuntu. I couldn't find out why. 
# The hint came from C:\gcc-linaro\share\doc\gcc-linaro-arm-linux-gnueabihf\README.txt 
#
# Uncomment the following line if you use Angstrom on your BeagleBone
#TARGET=angstrom

# Directory for C-Source
vpath %.c $(CURDIR)/source

# Directory for includes
CINCLUDE = $(CURDIR)/include  

# Directory for object files
OBJDIR = $(CURDIR)/object

# Other dependencies
DEPS = \
 Makefile \
 include/$(PROJECT).h \
 include/DFT_phasor.h \
 include/Seq_compnts.h \
 include/Freq_calc_phsr_ang.h \
 include/write_out_values.h \
 include/write_out_samples.h \
 include/write_out_all_values.h \
 include/P_Gain_dev_freq.h \
 include/adjust_phasor_angl.h \
 include/parameters_decoder.h \
 include/write_as_medplot_output_file.h
 

# Compiler object files 
COBJ = \
 $(OBJDIR)/$(PROJECT).o \
 $(OBJDIR)/DFT_phasor.o \
 $(OBJDIR)/Seq_compnts.o \
 $(OBJDIR)/Freq_calc_phsr_ang.o \
 $(OBJDIR)/write_out_values.o \
 $(OBJDIR)/write_out_samples.o \
 $(OBJDIR)/write_out_all_values.o \
 $(OBJDIR)/P_Gain_dev_freq.o \
 $(OBJDIR)/adjust_phasor_angl.o\
 $(OBJDIR)/parameters_decoder.o \
  $(OBJDIR)/write_as_medplot_output_file.o
 
 
 
# gcc binaries to use  (-lm is obrigatory for cross compile)
CC = "C:\gcc-linaro\bin\arm-linux-gnueabihf-gcc.exe" -lm
LD = "C:\gcc-linaro\bin\arm-linux-gnueabihf-gcc.exe" -lm

# rm is part of yagarto-tools
SHELL = cmd
REMOVE = rm -f

# Compiler options
# Two additional flags neccessary for Angstrom Linux. Don't use them with Ubuntu or Debian  
CFLAGS = -marm
ifeq ($(TARGET),angstrom)
CFLAGS += -march=armv4t
CFLAGS += -mfloat-abi=soft
endif
CFLAGS += -O0 
CFLAGS += -g 
CFLAGS += -I.
CFLAGS += -I$(CINCLUDE)
CFLAGS += $(CDEFINE)

# for a better output
MSG_EMPTYLINE = . 
MSG_COMPILING = ---COMPILE--- 
MSG_LINKING = ---LINK--- 
MSG_SUCCESS = ---SUCCESS--- 

# Our favourite
all: clean $(PROJECT)

# Linker call
$(PROJECT): $(COBJ)
	@echo Project name:
	@echo $(PROJECT)	
	@echo $(MSG_EMPTYLINE)
	@echo $(MSG_LINKING)
	@echo Current dir:
	@echo $(CURDIR)
	$(LD) -o $@ $^ $(CFLAGS)
	@echo $(MSG_EMPTYLINE)
	@echo $(MSG_SUCCESS) $(PROJECT)

# Compiler call
$(COBJ): $(OBJDIR)/%.o: %.c $(DEPS)
	@echo $(MSG_EMPTYLINE)
	@echo $(MSG_COMPILING) $<
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	$(REMOVE) $(OBJDIR)/*.o
	$(REMOVE) $(PROJECT)

