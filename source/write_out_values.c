/*
 * write_out_values.c
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#include <complex.h>
#include <stdio.h>
#include "write_out_all_values.h"


void write_out_values(float timestamp, struct phasor PhasorVA, struct phasor PhasorVB, struct phasor PhasorVC, float frequency, float complex Pos_seq, float complex Neg_seq, float complex Zer_seq )
{
    FILE *fp;	//File writing var
    register unsigned long int i = 1; //Iterations to mount the read vector.
    fp = fopen(C_OUTPUTS_FILENAME, "a"); //open in write mode

//////////////////////////////////////////// SAVING TIME/SOC /////////////////////////////////////////////////
    //timestamp
	fprintf(fp, "%f ",timestamp);

////////////////////////////////////// SAVING PHASORS (R +-jI) ///////////////////////////////////////////////
	//Phasor A
	fprintf(fp, "%f %fj ", PhasorVA.X_real, PhasorVA.X_imag);
	//Phasor B
	fprintf(fp, "%f %fj ", PhasorVB.X_real, PhasorVB.X_imag);
	//Phasor C
	fprintf(fp, "%f %fj ", PhasorVC.X_real, PhasorVC.X_imag);

////////////////////////////////////////// SAVING FREQUENCY //////////////////////////////////////////////////
    //frequency
	fprintf(fp, "%f ", frequency);
////////////////////////////////////// SAVING SEQUENCE COMPONENTS (R +-jI) ///////////////////////////////////////////////
	//Pos_seq
	fprintf(fp, "%f %fj ", creal(Pos_seq), cimag(Pos_seq));
	//Neg_seq
	fprintf(fp, "%f %fj ", creal(Neg_seq), cimag(Neg_seq));
	//Zer_seq
	fprintf(fp, "%f %fj ", creal(Zer_seq), cimag(Zer_seq));

	//break line
	fprintf(fp, "\n");

	fclose(fp);//Closing file



} //end of write_out_values
