/*
 * P_Gain_dev_freq.c
 *
 *  Created on: 18 de Mar de 2019
 *      Author: Maique Garcia
 */


//Gain P compensation
//Author : Maique C Garcia
//Description:
//			This function computes the Gain P parcel based on frequency deviation
//--------------------------------------------------------------------------------
//Requirements:
// Phases phasors
//--------------------------------------------------------------------------------
//Version control
// Vr number     /   Date   / Description
// 0.1           / 18-03-19 / Created initial version

//Utility links:
//http://aprs.gids.nl/nmea/#gga - Reference of frames

#include "P_Gain_dev_freq.h"
#include <errno.h> //http://www.virtsync.com/c-error-codes-include-errno CODES!!  //to debug values

float complex P_Gain_dev_freq(float est_freq, uint8_t sys_frequency)
{
	float complex P_GAIN ;
	float deltaw = 0;//, delta2 = 0, deltad = 0, dw_r=0;
	float twopi_x_fo = C_TWOPI * (float)sys_frequency, p1=0, p2 = 0,div_p=0;
	float pamos = 1/(float)(C_SAMPLES_PER_CYCLE * sys_frequency);

	deltaw = ((C_TWOPI * est_freq) -  twopi_x_fo);
	if (deltaw==0)
	{
		P_GAIN = 1;
	}
	else
	{
		p1=sin(( (float)C_SAMPLES_PER_CYCLE * deltaw * pamos) / 2);
		//	p1=sin(pamos);
		p2=(float)C_SAMPLES_PER_CYCLE * sin((deltaw * pamos) / 2);
		//	p2=sin((pamos*C_SAMPLES_PER_PERIOD)/2);
		div_p = p1 / p2;
		P_GAIN = div_p * cexp(I * (float)(C_SAMPLES_PER_CYCLE-1) * (deltaw * pamos) / 2);
	}
	return P_GAIN;
}
