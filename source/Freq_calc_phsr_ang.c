/*
 * Freq_calc_phsr_ang.c
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */
#include "Freq_calc_phsr_ang.h"

float Freq_calc_phsr_ang(float sig_in[3], uint8_t grid_frequency)
{

	float freq_output = 0.0;
	float delta1 = 0.0;
	float delta2 = 0.0;
	float deltad = 0.0;
	float dw_r=0.0;

	delta1 = sig_in[2]; //was 1
	delta2 = sig_in[0];
	deltad = delta2-delta1;

	if(deltad > C_PI)
	{
		deltad = deltad - C_TWOPI;
	}
	else if (deltad < (-1*C_PI))
	{
		deltad = deltad + C_TWOPI;
	}

	//Debug print parameters:
	//printf("angulos 0, 1 e 2: %f, %f e %f, deltad = %f , grid_frequency: %f,  \n\r", sig_in[0], sig_in[1], sig_in[2], deltad, (float)grid_frequency);

	//dw_r=(deltad*(float)grid_frequency*(float)C_SAMPLES_PER_CYCLE)/((float)(C_SAMPLES_PER_CYCLE*2));  //less efficiency
	//freq_output =(dw_r/C_TWOPI) + (float)grid_frequency ;
	dw_r=(deltad*(float)grid_frequency*(float)C_SAMPLES_PER_CYCLE)/(2*(float)C_SAMPLES_PER_CYCLE);

	freq_output = dw_r/C_TWOPI + (float)grid_frequency ;
	//printf("dw_r: %f, freq output: %f \n\r", dw_r, freq_output);


	return freq_output;

}
