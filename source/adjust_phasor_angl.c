/*
 * adjust_phasor_angl.c
 *
 *  Created on: 16 de jul de 2018
 *      Author: Maique Garcia
 */


#include <math.h>
#include <complex.h>
#include "adjust_phasor_angl.h"


#ifndef SOURCE_ADJUST_PHASOR_ANGL_C_
#define SOURCE_ADJUST_PHASOR_ANGL_C_

/*
 * Angle adjust is an input value to be ajusted in radians
 */
struct phasor adjust_phasor_angl ( struct phasor phasor_in ,float MagAdjust , float angle_adjust)
{
    struct phasor adjusted_phasor;
    float phasor_mag,old_angle, new_angle;

    old_angle = atan2(phasor_in.X_real, phasor_in.X_imag);
    phasor_mag = (sqrt( (phasor_in.X_imag*phasor_in.X_imag)+(phasor_in.X_real*phasor_in.X_real))) + MagAdjust;

    new_angle = old_angle + angle_adjust;

    adjusted_phasor.X_imag = cos(new_angle)*phasor_mag;
    adjusted_phasor.X_real = sin(new_angle)*phasor_mag;


    return adjusted_phasor;
}; //end function



#endif /* SOURCE_ADJUST_PHASOR_ANGL_C_ */
