/*
 * write_out_samples.c
 *
 *  Created on: 27 de abr de 2018
 *      Author: Maique Garcia
 */

/*
 * write_out_values.c
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#include <stdio.h>
#include "write_out_samples.h"


void write_out_samples(uint16_t cont_amos[C_SAMPLES_PER_CYCLE],  float time[C_SAMPLES_PER_CYCLE], float tensao_fa[C_SAMPLES_PER_CYCLE], float tensao_fb[C_SAMPLES_PER_CYCLE], float tensao_fc[C_SAMPLES_PER_CYCLE] ){

	FILE *fp_debug;	//File writing var
    register unsigned long int i = 1; //Iterations to mount the read vector.
    fp_debug = fopen(C_OUTPUTS_FILENAME_SAMPLES, "a"); //open in write mode

    for (i = 0; i< C_SAMPLES_PER_CYCLE; i++)
    {
    	//////////////////////////////////////////// SAVING TIME/SOC /////////////////////////////////////////////////
		//contamos
		fprintf(fp_debug, "%d ", cont_amos[i]);
		//time
		fprintf(fp_debug, "%f ", time[i]);
		////////////////////////////////////// SAVING SAMPLES ///////////////////////////////////////////////
		//Phasor A
		fprintf(fp_debug, "%f ", tensao_fa[i]);
		fprintf(fp_debug, "%f ", tensao_fb[i]);
		fprintf(fp_debug, "%f ", tensao_fc[i]);


		fprintf(fp_debug, "\n");
    }

	fclose(fp_debug);//Closing file

} //end of write_out_values

