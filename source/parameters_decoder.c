/*
 * decode_D-PMU_file_parameters.c
 *
 *  Created on: 17 de jun de 2018
 *      Author: Maique Garcia
 */

#include "./include/parameters_decoder.h"
#include <stdio.h>

#define C_PARAMETER_FILE "/usr/D-PMU_parameters/D-PMU_parameters.txt"

struct dpmu_file_decoded_st interpreted_data; //store data interpreted from the input file.

void decode_DPMU_file_parameters_pmu(void){

	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	size_t length, length_header;


	//Open file
	fp = fopen(C_PARAMETER_FILE, "r");
	//check file health
	if (fp == NULL)
	{
		printf("Error. The Parameters file is empty.");
		exit(EXIT_FAILURE);
	}

	//structure reset.
	memset(&interpreted_data, 0x00, sizeof(interpreted_data));

	//Read and decode important lines (with '*' identifier )
	while ((length = getline(&line, &len, fp)) != -1) //seek file until it ends
	{

		if(line[0] == '*') //parameter identifier
		{
			if(strncmp(line,"*D-PMU_ANGLE_ADJUST_PHASOR_A",28) == 0)
			{
				length = getline(&interpreted_data.angle_adjust_constant[0], &len, fp);
				printf("D-PMU angle adjust for Phasor A: %s\r\n", interpreted_data.angle_adjust_constant[0]);
			}
			else if(strncmp(line,"*D-PMU_ANGLE_ADJUST_PHASOR_B",28) == 0)
			{
				length = getline(&interpreted_data.angle_adjust_constant[1], &len, fp);
				printf("D-PMU angle adjust for Phasor B: %s\r\n", interpreted_data.angle_adjust_constant[1]);
			}
			else if(strncmp(line,"*D-PMU_ANGLE_ADJUST_PHASOR_C",28) == 0)
			{
				length = getline(&interpreted_data.angle_adjust_constant[2], &len, fp);
				printf("D-PMU angle adjust for Phasor C: %s\r\n", interpreted_data.angle_adjust_constant[2]);
			}
			else if(strncmp(line,"*NUMBER_OF_PHASORS_TO_SEND",26)== 0)
			{
				length = getline(&line, &len, fp);
				printf("Number of phasors to be sent: %s\r\n", line);
				if(strncmp(line,"7",1) == 0)
				{
					interpreted_data.number_of_phasors = 7;
				}
				else if(strncmp(line,"6",1) == 0)
				{
					interpreted_data.number_of_phasors = 6;
				}
				else
				{
					interpreted_data.number_of_phasors = 3;
				}
			}
			else if(strncmp(line,"*PHASOR_REFERENCE_FOR_FREQ_ESTIMATION",36)== 0)
			{
				length = getline(&line, &len, fp); //take the value from PDC destiny ip
				printf("Frequency estimation reference phasor: %s\r\n", line);

				if(strncmp(line,"phasor_a",8) == 0)
				{
					interpreted_data.phasor_ref_mode = FREQ_OBTAINED_FROM_PHASE_A_PHASOR;
				}
				else if (strncmp(line,"phasor_b",8) == 0)
				{
					interpreted_data.phasor_ref_mode = FREQ_OBTAINED_FROM_PHASE_B_PHASOR;
				}
				else
				{
					interpreted_data.phasor_ref_mode = FREQ_OBTAINED_FROM_POS_SEQ_PHASOR;
				}
			}
			else if(strncmp(line,"*STIMULUS_SOURCE",16) == 0)
			{
				length = getline(&line, &len, fp); //take response from stimulus source
				if(strncmp(line,"use_stimulus_file",17) == 0)
				{
					interpreted_data.stimulus_mode = 1;
				}
				printf("D-PMU stimulus source (0-PRUSS source, 1-Stimulus file): %d\r\n", interpreted_data.stimulus_mode);
			}
			else if (strncmp(line,"*NOMINAL_SYS_FREQUENCY",22) == 0)
			{
				length = getline(&line, &len, fp);
				interpreted_data.nominal_sys_freq = atoi(line);
				printf("Nominal configured frequency: %d\r\n", interpreted_data.nominal_sys_freq);
			}

			else
			{

			}
		}

	}
	puts("--------------------Decoding process finished--------------------------------");

	fclose(fp);
	//if (line)
	//	free(line);
	//exit(EXIT_SUCCESS);

};


/*Function  void return_dpmu_freq_estimation_mode(void)
 * Brief	This function get the D-PMU frequency estimation mode
 * Param	none
 * Return 	freq_phasor_estimation_mode_enum
 */
freq_phasor_estimation_mode_enum return_dpmu_freq_estimation_mode(void)
{
	return  interpreted_data.phasor_ref_mode;
}

/*Function  void return_stimulus_source_mode(uint8_t stimulus_mode)
 * Brief	This function get the D-PMU magnitudes stimulus mode behavior
 * Param	pointer to byte flag to be write.
 * Return 	stimulus_mode
 */
uint8_t return_dpmu_stimulus_mode()
{

	return interpreted_data.stimulus_mode;
}

/*Function  void return_dpmu_angle_adjusts(float *angles_adjust[3])
 * Brief	This function get the D-PMU angle adjust for 3 phasors.
 * Param	pointer to float structure to be write.
 * Return 	none
 */
void return_dpmu_angle_adjusts(float *angles_adjust)
{
	float temp;

	angles_adjust[0] = (float)atof(interpreted_data.angle_adjust_constant[0]);
	angles_adjust[1] = atof(interpreted_data.angle_adjust_constant[1]);
	angles_adjust[2] = atof(interpreted_data.angle_adjust_constant[2]);
}

uint8_t return_dpmu_number_of_phasors(void)
{
	if (interpreted_data.number_of_phasors == 0)
	{
		return 3; //Default value
	}
	else
	{
		return interpreted_data.number_of_phasors; //Parameter decoded value
	}
}


uint8_t return_dpmu_nominal_freq()
{
	return interpreted_data.nominal_sys_freq;
}

//
//
//
//char *return_dpmu_pdc_destinyport(void)
//{
//	return interpreted_data.pdc_port;
//}
//
//
//
///* Return the status of configuration to secondary PDC IP*/
////Return 1 to enabled secondary IP
////Return 0 to disabled secondary IP
//uint8_t return_dpmu_enabled_scnd_pdc(void)
//{
//	if (send_data_other_PDC == 1)
//	{
//		return TRUE;
//	}
//	else
//	{
//		return FALSE;
//	}
//}
//
//
////Call that only if secndary ip is enabled.
//char *return_dpmu_scnd_pdc_destinyip(void)
//{
//	return interpreted_data.scnd_pdc_ip;
//}
//
//
//
//char *return_dpmu_scnd_pdc_destinyport(void)
//{
//	return interpreted_data.scnd_pdc_port;
//}
