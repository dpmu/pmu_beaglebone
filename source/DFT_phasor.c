/*
 * DFT_phasor.c
 *
 *  Created on: 23 de abr de 2018
 *      Author: Maique Garcia
 */

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "DFT_phasor.h"

struct phasor DFT_phasor ( float discr_sig[] , int point_dft , uint8_t cycle_points_type)   // function definition
{
    /*CAREFUL WITH VECTOR LENGTH AND K AN N VARIABLES TYPES*/
    register unsigned int k; //loop iteration variable
    register unsigned int n; //loop iteration variable
    struct phasor DFT_out ;
    double complex XN_DFT[C_SAMPLES_PER_CYCLE];
    int j=0;
   // puts("starting DFT");

	//for (k = 1; k < point_dft; k++){ //WITHOUT DC CALCULUS - k=0
    k=1; //fundamental frequency phasor calc only
    XN_DFT[k] = 0 + 0*I;

    if (cycle_points_type == 1) //Dont used after version 2.0, this value isn't set on cycle_points_type
    {
        puts("Cycle1");
        for (n = 0; n < 16 ; n++){  ///AJUSTAR PARA 0 A xn_len-1
            XN_DFT[k] = XN_DFT[k] + discr_sig[n] * cexp(-I * C_PI * (n+1) / (C_SAMPLES_PER_CYCLE>>1)); //compensated to half cycle DFT  -calculate above 2� cycle part
        };//n
        DFT_out.X_real = (float)(creal(XN_DFT[1]) / (C_SAMPLES_PER_CYCLE>>2)) / sqrt(2);
        DFT_out.X_imag = (float)(cimag(XN_DFT[1]) / (C_SAMPLES_PER_CYCLE>>2)) / sqrt(2);


    }else if(cycle_points_type==2){
        puts("Cycle 2");
        for (n = 0; n < 16 ; n++){  ///AJUSTAR PARA 0 A xn_len-1
            XN_DFT[k] = XN_DFT[k] + discr_sig[n+15] * cexp(-I * C_PI * (n+16) / (C_SAMPLES_PER_CYCLE>>1)); //compensated to half cycle DFT  -calculate above 2� cycle part
        };//n
        DFT_out.X_real = (float)(creal(XN_DFT[1]) / (C_SAMPLES_PER_CYCLE>>2)) / sqrt(2);
        DFT_out.X_imag = (float)(cimag(XN_DFT[1]) / (C_SAMPLES_PER_CYCLE>>2)) / sqrt(2);
    }
    else
    {

        //MATLAB DFT
        float delta = ((float)C_TWOPI / C_SAMPLES_PER_CYCLE);
        float constant = sqrt(2) / C_SAMPLES_PER_CYCLE;
        float complex sum = 0 + 0*I;
        for(n=0; n<=(C_SAMPLES_PER_CYCLE-1); n++)
        {
        	sum = sum + (discr_sig[n] * cexp(-I * delta * (n)));
        }
        XN_DFT[1] =  constant * sum;
        //printf("DFT AS MATLAB XN_DFT[2] is  %f %fi\n",creal(XN_DFT[1]),cimag(XN_DFT[1]));
        //printf("Parameters are: delta = %f; constant = %f, sum = %f + %fj\n",delta, constant, creal(sum),cimag(sum));
        DFT_out.X_real = (float)creal(XN_DFT[1]);
        DFT_out.X_imag = (float)cimag(XN_DFT[1]);
    }

    return DFT_out;
}
