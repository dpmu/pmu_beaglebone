/*
 * write_as_medplot_output_file.c
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#include <complex.h>
#include <stdio.h>
#include <math.h>
#include <write_as_medplot_output_file.h>

void write_as_medplot_output_file(float timestamp, struct phasor PhasorVA, struct phasor PhasorVB, struct phasor PhasorVC, float frequency, float ROCOF){
    FILE *fp;	//File writing var
    register unsigned long int i = 1; //Iterations to mount the read vector.
    float PhasorVA_mag, PhasorVA_angl;
    float PhasorVB_mag, PhasorVB_angl;
    float PhasorVC_mag, PhasorVC_angl;


    fp = fopen(C_OUTPUTS_FILENAME, "a"); //open in write mode


//////////////////////////////////////////// SAVING TIME/SOC /////////////////////////////////////////////////
    //timestamp
	fprintf(fp, "%f       ",timestamp);

////////////////////////////////////// SAVING PHASORS (R +-jI) ///////////////////////////////////////////////
	//Phasor A (mag and angle)
	PhasorVA_mag = (float)sqrt((PhasorVA.X_real*PhasorVA.X_real) + (PhasorVA.X_imag*PhasorVA.X_imag) ) ;
	printf("PhasorVA COMP is %f + j%f \n",PhasorVA.X_real, PhasorVA.X_imag);

	PhasorVA_angl = atan2(PhasorVA.X_imag, PhasorVA.X_real)*180/C_PI;
	fprintf(fp, "%f      ", PhasorVA_mag);
	fprintf(fp, "%f      ", PhasorVA_angl);


	//Phasor B
	PhasorVB_mag = (float)sqrt((PhasorVB.X_real*PhasorVB.X_real) + (PhasorVB.X_imag*PhasorVB.X_imag) ) ;
	//printf("PhasorVB COMPlemento is %f + j%f \n",PhasorVB.X_real, PhasorVB.X_imag);

	PhasorVB_angl = atan2(PhasorVB.X_imag, PhasorVB.X_real)*180/C_PI;
	fprintf(fp, "%f      ", PhasorVB_mag);
	fprintf(fp, "%f      ", PhasorVB_angl);

	//Phasor C
	PhasorVC_mag = (float)sqrt((PhasorVC.X_real*PhasorVC.X_real) + (PhasorVC.X_imag*PhasorVC.X_imag) ) ;
	PhasorVC_angl = atan2(PhasorVC.X_imag, PhasorVC.X_real) *180/C_PI;
	fprintf(fp, "%f      ", PhasorVC_mag);
	fprintf(fp, "%f      ", PhasorVC_angl);

////////////////////////////////////////// SAVING FREQUENCY //////////////////////////////////////////////////
    //frequency
	fprintf(fp, "%f      ", frequency);

////////////////////////////////////////// SAVING ROCOF  //////////////////////////////////////////////////
	//ROCOF
	fprintf(fp, "%f      ", ROCOF);

////////////////////////////////////////// Frame Faltant //////////////////////////////////////////////////
	//Flag faltante
	fprintf(fp, "           ");

	//break line
	fprintf(fp, "\r\n");


	fclose(fp);//Closing file



}; //end of write_out_values
