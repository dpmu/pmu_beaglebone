/*
 * write_out_all_values.c
 *
 *  Created on: 14 de jun de 2018
 *      Author: Maique Garcia
 */


/*
 * write_out_values.c
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#include <complex.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include "write_out_all_values.h"


int write_out_all_values(uint16_t timestamp[], float PhaseVA[], float PhaseVB[], float PhaseVC[] ,struct phasor PhasorVA, struct phasor PhasorVB, struct phasor PhasorVC,  float complex Pos_seq, float complex Neg_seq, float complex Zer_seq, float estimated_freq, int saved_counts, FILE *fp){

	register unsigned long int i = 1; //Iterations to mount the read vector.


	if (C_BUFFER_CIRCULAR == 1)  //Change file
	{

		for (i=0; i < C_SAMPLES_PER_CYCLE; i++){
		//////////////////////////////////////////// SAVING TIME/SOC /////////////////////////////////////////////////
			//timestamp
			fprintf(fp, "%d ",timestamp[i]);

		////////////////////////////////////// SAVING PHASES (va, vB, vC) ///////////////////////////////////////////////
			//Phase A
			fprintf(fp, "%f ",  PhaseVA[i]);
			//Phase B
			fprintf(fp, "%f ",  PhaseVB[i]);
			//Phase C
			fprintf(fp, "%f ",  PhaseVC[i]);
		////////////////////////////////////// SAVING PHASORS (R +-jI) ///////////////////////////////////////////////
			//Phasor A
			fprintf(fp, "%f %fj ", PhasorVA.X_real, PhasorVA.X_imag);
			//Phasor B
			fprintf(fp, "%f %fj ", PhasorVB.X_real, PhasorVB.X_imag);
			//Phasor C
			fprintf(fp, "%f %fj ", PhasorVC.X_real, PhasorVC.X_imag);

		////////////////////////////////////////// SAVING FREQUENCY //////////////////////////////////////////////////
			//frequency
			fprintf(fp, "%f ", estimated_freq);
		////////////////////////////////////// SAVING SEQUENCE COMPONENTS (R +-jI) ///////////////////////////////////////////////
			//Pos_seq
			fprintf(fp, "%f %fj ", creal(Pos_seq), cimag(Pos_seq));
			//Neg_seq
//			fprintf(fp, "%f %fj ", creal(Neg_seq), cimag(Neg_seq));
			//Zer_seq
//			fprintf(fp, "%f %fj ", creal(Zer_seq), cimag(Zer_seq));

			//break line
			fprintf(fp, "\n");

		};


	}
	else
	{


		for (i=0; i < C_SAMPLES_PER_CYCLE; i++){
		//////////////////////////////////////////// SAVING TIME/SOC /////////////////////////////////////////////////
			//timestamp
			fprintf(fp, "%f ",timestamp[i]);

		////////////////////////////////////// SAVING PHASES (va, vB, vC) ///////////////////////////////////////////////
			//Phase A
			fprintf(fp, "%f ",  PhaseVA[i]);
			//Phase B
			fprintf(fp, "%f ",  PhaseVB[i]);
			//Phase C
			fprintf(fp, "%f ",  PhaseVC[i]);
		////////////////////////////////////// SAVING PHASORS (R +-jI) ///////////////////////////////////////////////
			//Phasor A
			fprintf(fp, "%f %fj ", PhasorVA.X_real, PhasorVA.X_imag);
			//Phasor B
			fprintf(fp, "%f %fj ", PhasorVB.X_real, PhasorVB.X_imag);
			//Phasor C
			fprintf(fp, "%f %fj ", PhasorVC.X_real, PhasorVC.X_imag);

		////////////////////////////////////////// SAVING FREQUENCY //////////////////////////////////////////////////
			//frequency
			fprintf(fp, "%f ", estimated_freq);
		////////////////////////////////////// SAVING SEQUENCE COMPONENTS (R +-jI) ///////////////////////////////////////////////
			//Pos_seq
			fprintf(fp, "%f %fj ", creal(Pos_seq), cimag(Pos_seq));
			//Neg_seq
			fprintf(fp, "%f %fj ", creal(Neg_seq), cimag(Neg_seq));
			//Zer_seq
			fprintf(fp, "%f %fj ", creal(Zer_seq), cimag(Zer_seq));

			//break line
			fprintf(fp, "\n");
		}
	}
	return (saved_counts+1);

} //end of this program
