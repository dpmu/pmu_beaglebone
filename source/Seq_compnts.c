/*
 * Seq_compnts.c
 *
 *  Created on: 24 de abr de 2018
 *      Author: Maique Garcia
 */

#include <math.h>
#include <complex.h>
#include "Seq_compnts.h"

struct SEQ_COMPONETS Seq_compnts(struct phasor Phasor_VA, struct phasor Phasor_VB, struct phasor Phasor_VC)
{
	struct SEQ_COMPONETS COMPONENTS;
	float complex positive_seq;
	float complex negative_seq, zero_seq;
	float complex complex_VA = Phasor_VA.X_real + Phasor_VA.X_imag*I;
	float complex complex_VB = Phasor_VB.X_real + Phasor_VB.X_imag*I;
	float complex complex_VC = Phasor_VC.X_real + Phasor_VC.X_imag*I;

	float complex alph_x_VB = (creal(C_COMP_ALPH)*creal(complex_VB)-(cimag(C_COMP_ALPH)*cimag(complex_VB))) + (creal(C_COMP_ALPH)*cimag(complex_VB)+cimag(C_COMP_ALPH)*creal(complex_VB))*I;
	float complex alph2_x_VB = (creal(C_COMP_ALPH2)*creal(complex_VB)-(cimag(C_COMP_ALPH2)*cimag(complex_VB))) + (creal(C_COMP_ALPH2)*cimag(complex_VB)+cimag(C_COMP_ALPH2)*creal(complex_VB))*I;
	float complex alph_x_VC = (creal(C_COMP_ALPH)*creal(complex_VC)-(cimag(C_COMP_ALPH)*cimag(complex_VC))) + (creal(C_COMP_ALPH)*cimag(complex_VC)+cimag(C_COMP_ALPH)*creal(complex_VC))*I;
	float complex alph2_x_VC =(creal(C_COMP_ALPH2)*creal(complex_VC)-(cimag(C_COMP_ALPH2)*cimag(complex_VC))) + (creal(C_COMP_ALPH2)*cimag(complex_VC)+cimag(C_COMP_ALPH2)*creal(complex_VC))*I;


	//Positive sequence calculus  ->  1/3*(A +alph*B + alph^2*C)
	positive_seq = (complex_VA + alph_x_VB + alph2_x_VC);

	//Negative sequence calculus  ->  1/3*(A +alph^2*B + alph*C)
	negative_seq = (complex_VA + alph2_x_VB + alph_x_VC);

	//Zero sequence calculus  ->  1/3*(A + B + C)
	zero_seq = (complex_VA + complex_VB + complex_VC);

	COMPONENTS.Pos_seq = positive_seq / 3;
	COMPONENTS.Neg_seq = negative_seq / 3;
	COMPONENTS.Zer_seq = zero_seq / 3;

	return COMPONENTS;

};//Function end
