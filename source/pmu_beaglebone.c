/*
 * pmu_beaglebone.c
 *
 *  Created on: 23 de abr de 2018
 *      Author: Maique Garcia
 */
//Fonte de cria��o do programa  :  http://jkuhlm.bplaced.net/hellobone/

/*
Version Control
// Vr number     /   Date   / Description
//1.0			/ 15-06-18  / Working version of the PMU beaglebone without C37118 program - just with this fifo connection
//1.1			/ 19-06-18  / Starting the adequation of serial interpretation of SOC from GPS and with process slice using fork
//1.2			/ 25-06-18  / Fixed the SOC assertion with +1 on the SOC received assertion. Because the serial is received now, but the SOC with this value will be sent on next cycle (SOC variable update).
//1.3			/ 05-06-18  / Fixed the DFT k indices. They must be compliant with 0 to N-1, not 1 to N.
//1.4			/ 16-07-18  / Inserted the angle compensation parcel on each calculated phasor
//1.5			/ 27-07-18  / Implementing the Reason RT430 NMEA GPZDA frame support - Frame example: $GPZDA,191922.0,27,07,2018,,*5F
//1.6			/ 28-07-18  / Implementing the Flag to identify the SOC attribution compensation value (If Reason, because the delays of FIFOs, the SOC calculus should be update on the next PPS-count=1 wothout +1 compensation). On SL869 the SOC is updated on the next cout=1 (with +1 compensation)
//1.8			/ 04-07-18	/ Removing the Positive sequence component flow to PDC.
//1.9			/ 06-07-18  / ROCOF implementation (as defined in C37.118.1-2011) ALSO CHANGING the frequency and ROCOF from fixed to float type variables (changing format filed too)
//2.0			/ 15-01-19	/ Changing the source code to efficient implementation
//2.1			/ 08-02-19	/ Implementing a new frac_sec update method. At beginning with counter counter samples acquisition from FIFO
//2.2			/ 11-03-20  / Moving the open-close of serial port from inside the "soc_time_calculated_GPS_process" to main process task
//2.3			/ 26-03-20  / Starting the parameter file decoder support.
//2.4			/ 08-04-20  / Removed the out-project defines file.
//2.5			/ 25-04-20  / Added option to choose phasor A or Positive Sequence angle for Frequency estimation
//2.6			/ 13-10-20  / Restored capability to use stimulus file as samples source and save the synchrophasors into output file.
//2.7			/ 28-10-20  / Fixing the 60 repeated CFG-2 frames sent to C_37118 program on first second
//2.8			/ 28-11-20	/ Added 50 Hz grid support feature
//2.9			/ 04-03-22  / Adding frequency estimation from phasor B. Sometimes it's necessary.
*/

//////////////////////////////////MACROS to save cycles ////////////////////////////////////////////////
#define CLR_VAL(p) ((p) = 0)  //clear p position n
#define COMPARE_VAL(a,b) (a==b ? 1 : 0) // compares 2 values

#include "../include/pmu_beaglebone.h"
#include "../include/parameters_decoder.h"
#include <errno.h> //http://www.virtsync.com/c-error-codes-include-errno CODES!!  //to debug values
#include<termios.h>  //used to decode and access the serial port on BBB
#include <syslog.h> //sys log file rotation

//See values on
//cat /var/log/syslog | grep Samples_handler
//write on file in this form '   syslog(LOG_ERR, "MAIQUE TESTE Syslog", valuesss )  ';

int main(void) {
	puts("PMU prototype - starting V2.9");
	syslog(LOG_INFO, "Running the pmu_beaglebone program version %f", 2.9);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Variables and structures for this program									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	float _adjust_phasors_angle[3];
    float complex p_gain_compensation;
    struct PMU_FINAL_DATA final_data_to_protocol;
    struct meter_values meter;//, registered_values;
    float meter_buffer[SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE]; ///meter buffer on fifo
    register uint16_t j=0;//, iteration_read = 0, last_j_iteration = 0;
    bool dft_calc_allow = false, freq_estimation_allow = false;
    struct phasor phasor_Va, phasor_Vb, phasor_Vc; //Phasors of DFT output
    uint8_t cycle_points_type = 0 ; //Cycle points type -> 0 = Full points DFT, 1 = Half period DFT using first Nw/2 samples, 2 = Half period DFT using last Nw/2 samples
    struct SEQ_COMPONETS seq_components;
    float estimated_freq[2],estimated_rocof;
    float freq_angle_reference[3] = {0.0, 0.0, 0.0};
    uint32_t frac_sec=0;
    uint8_t first_iteration_flag = 1;
    uint8_t iter = 0;
    uint8_t number_of_phasors_to_send;
    uint8_t fifo_size_in_bytes;
    uint8_t _stimulus_mode; //refers to stimulus obtention mode (PRUSS if 0, stimulus file if 1).
    freq_phasor_estimation_mode_enum phasor_reference_for_freq_mode = FREQ_OBTAINED_FROM_POS_SEQ_PHASOR;
    uint8_t _grid_nominal_freq = 60;
    uint8_t frac_sec_adj = 10;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Open and mount C37118 parameters structure									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	decode_DPMU_file_parameters_pmu();
	_stimulus_mode = return_dpmu_stimulus_mode();
	return_dpmu_angle_adjusts(&_adjust_phasors_angle);
	_grid_nominal_freq = return_dpmu_nominal_freq();
	if (_grid_nominal_freq == 50)
	{
		frac_sec_adj = 12;
	}
	else
	{
		frac_sec_adj = 10;
	}

	printf("Phasors Angle adjust: A: %f  B: %f  C: %f \r\n", _adjust_phasors_angle[0], _adjust_phasors_angle[1], _adjust_phasors_angle[2]);
	number_of_phasors_to_send = return_dpmu_number_of_phasors();
	if (number_of_phasors_to_send == 7)
	{
		fifo_size_in_bytes = 52;
	}
	else
	{
		fifo_size_in_bytes = 44;
	}
	phasor_reference_for_freq_mode = return_dpmu_freq_estimation_mode();
	printf("Phasor ref for frequency is %d \r\n", (uint8_t)phasor_reference_for_freq_mode);

	//exit(EXIT_SUCCESS); //debug

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Structure pointers for this program									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    uint8_t *FREQ_pointer_to_char_representation = (uint8_t*)&(final_data_to_protocol.FREQ);
    uint8_t *DFREQ_pointer_to_char_representation = (uint8_t*)&(final_data_to_protocol.DFREQ);
    uint8_t *SOC_pointer_to_char_repres =  (uint8_t*)&(final_data_to_protocol.SOC);
    uint8_t *FRACSEC_pointer_to_char_repres = (uint8_t*)&(final_data_to_protocol.FRACSEC);
    uint8_t *TIME_BASE_pointers 	= 	(uint8_t*)&(final_data_to_protocol.TIME_BASE);
    uint8_t *STAT_DATA_pointers 	= 	(uint8_t*)&(final_data_to_protocol.STAT_DATA);
    uint8_t *PhsA_real_pointers 	= 	(uint8_t*)&(final_data_to_protocol.PhsA_real);
    uint8_t *PhsA_imag_pointers 	= 	(uint8_t*)&(final_data_to_protocol.PhsA_imag);
    uint8_t *PhsB_real_pointers 	= 	(uint8_t*)&(final_data_to_protocol.PhsB_real);
    uint8_t *PhsB_imag_pointers 	= 	(uint8_t*)&(final_data_to_protocol.PhsB_imag);
    uint8_t *PhsC_real_pointers 	= 	(uint8_t*)&(final_data_to_protocol.PhsC_real);
    uint8_t *PhsC_imag_pointers 	= 	(uint8_t*)&(final_data_to_protocol.PhsC_imag);
    uint8_t *PhsPpos_real_pointers 	= 	(uint8_t*)&(final_data_to_protocol.Pos_seq_real);
    uint8_t *PhsPpos_imag_pointers 	= 	(uint8_t*)&(final_data_to_protocol.Pos_seq_imag);


	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 									PMU to C37118 FIFO interface get									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	int res_protocol,c37118_fifo_fd;
	uint8_t c37118_pipe[fifo_size_in_bytes];
	int open_mode_protocol = O_WRONLY | O_NONBLOCK;//O_RDONLY; //O_NOMBLOCK serve para n�o bloquear a escrita na fifo e permite debugar quando ela esta cheia por conta da variavel de retorno (=-1 -> fifo full; = 255 -> fifo ok)
#ifndef _PMU_WORKS_WITHOUT_C37118
	if ( _stimulus_mode == 0 )
	{
		int open_mode_protocol = O_WRONLY | O_NONBLOCK;//O_RDONLY; //O_NOMBLOCK serve para n�o bloquear a escrita na fifo e permite debugar quando ela esta cheia por conta da variavel de retorno (=-1 -> fifo full; = 255 -> fifo ok)
		printf("Oppening C37118 FIFO\n");
		memset(c37118_pipe, '\0', sizeof(c37118_pipe));
		c37118_fifo_fd = open(C_FIFO_PATHNAME_SERVER, open_mode_protocol);
		if (c37118_fifo_fd == -1)
		{
			printf("PMU program : failed on open FIFO pmu2c37");
			exit(EXIT_FAILURE);
		}
	}
#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 									Meter to PMU FIFO interface open									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	int meter_pipe_fd;
	int ADC_saved_counts = 0 ;
	int res;
	int open_mode = O_RDONLY;//O_WRONLY;  //only read because the Samples Handler is the only one that can write
	//directory verificatiion - Avoid fail or ftok and reseting pipe
	if (mkdir(C_FIFO_PATH,777) == -1){
		remove(C_FIFO_PATH);
	};
	mkdir(C_FIFO_PATH,777); //always re/create the file

	if (access(C_FIFO_PATHNAME, F_OK) == -1) {
		res = mkfifo(C_FIFO_PATHNAME, 0777);
		if (res != 0) {
			fprintf(stderr, "PMU program :Could not create fifo handler2pmu %s\n", C_FIFO_PATHNAME);
			exit(EXIT_FAILURE);
		}
	}
	// printf("Process %d opening FIFO O_WRONLY\n", getpid());
	puts("Waiting handler FIFO");
	meter_pipe_fd = open(C_FIFO_PATHNAME, open_mode);
	if (meter_pipe_fd == -1) {
		printf("PMU program :failed on open FIFO handler2pmu");
		exit(EXIT_SUCCESS);
	}

	if ( _stimulus_mode == 1)
	{
		remove(C_OUTPUTS_FILENAME);
		//Adding 8 dump first lines on output file(when offline mode)
		for (j=0; j<=7;j++)
		{
			write_as_medplot_output_file(meter.time[0], phasor_Va, phasor_Vb, phasor_Vc, estimated_freq[0], estimated_rocof);
		}
	}



	puts("Phasor estimation program running.");


	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 											Infinite Loop												//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	while(1){

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 							Reading and decode data from SamplesHandler PIPE							//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		for (j=0; j <= (C_SAMPLES_PER_CYCLE-1); j++){

			res = read(meter_pipe_fd, meter_buffer, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE); //20 is compliant with 5 values in float in 1 full cycle representation
			//printf("feedback from read pipe = %d \n", res);

			if (res <= -1){
				printf("PROBLEM DETECTED");
				printf("errno value: %d \n", errno);
				syslog(LOG_ERR, "Problem on reading samples from sampler handler fifo. Errno: %d", errno);

			}else if(res == 0){
				printf("PMU reading fifo closed, Closing APP.\n");
				syslog(LOG_ERR, "PMU to sampler handler fifo closed. Closing app. ERRNO = 0");
				exit(EXIT_SUCCESS);
			};

			 meter.cont_amos[j] 	= (uint16_t)meter_buffer[0] ; // counter from samples handler
			 if (_stimulus_mode == 1)
			 {
				 meter.time[j] = meter_buffer[0];						//Float version of samples counter (necessary to save as time on output file)
				 if( ((meter_buffer[0] == meter_buffer[1]) && (meter_buffer[2] == meter_buffer[3])) || (meter_buffer[4] != 0) ){   //Way to detect the end  of file (the samples handler send -1 for all parameters) or EOF arrives
					 printf("EOF arrived, closing phasor estimator.\n");
					 exit(EXIT_SUCCESS); //ending program (the samples handler send -1-1-1-1 (same for all values)
				 }
			 }
			 meter.tensao_fa[j] 	= meter_buffer[1];
			 meter.tensao_fb[j] 	= meter_buffer[2];
			 meter.tensao_fc[j] 	= meter_buffer[3];
			 //meter.soc[j] =	(uint32_t)meter_buffer[4];							//SOC
			 memcpy(&meter.soc[0], &meter_buffer[4], 4);							//Used Memcopy because the data truncation caused by the uint32_t -> float format conversion
			 //Space for currents - future implementation:
		};


		if ( _stimulus_mode == 0) //Read data from ADC.
		{

			//Counter number verification Verification
			if( (meter.cont_amos[C_SAMPLES_PER_CYCLE-1] % 64) != 0 ){
				syslog(LOG_ERR, "Reference samples counter has shifted. Debug: counter[0] is %d, counter[1] is %d, ... , counter[62] is %d, counter[63] is %d", meter.cont_amos[0] , meter.cont_amos[1],  meter.cont_amos[62], meter.cont_amos[63]);
				puts("the counter has shifted");
				printf("\n sample[0] is %d, sample[1] is %d, ... , sample[62] is %d, sample[63] is %d \n", meter.cont_amos[0] , meter.cont_amos[1],  meter.cont_amos[62], meter.cont_amos[63]);
				printf("\n VA[0] is %f, VA[1] is %f, ... , VA[62] is %f, VA[63] is %f\n", meter.tensao_fa[0] , meter.tensao_fa[1],  meter.tensao_fa[62], meter.tensao_fa[63] );
				//printf("\n VB[0] is %f, VB[1] is %f, ... , VB[62] is %f, VB[63] is %f\n", meter.tensao_fb[0] , meter.tensao_fb[1],  meter.tensao_fb[62], meter.tensao_fb[63] );
				//printf("\n VC[0] is %f, VC[1] is %f, ... , VC[62] is %f, VC[63] is %f\n", meter.tensao_fc[0] , meter.tensao_fc[1],  meter.tensao_fc[62], meter.tensao_fc[63] );
				//resynchronising the meter vector (first simple, first position)

				while(meter_buffer[0] !=1){
					res = read(meter_pipe_fd, meter_buffer, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE); //20 is compliant with 5 values in float in 1 full cycle representation
				}

				frac_sec = 0; //Fractional second part reseting
				meter.cont_amos[0] 	= (uint16_t)meter_buffer[0] ; // counter from samples handler
				meter.tensao_fa[0] 	= meter_buffer[1];
				meter.tensao_fb[0] 	= meter_buffer[2];
				meter.tensao_fc[0] 	= meter_buffer[3];
				memcpy(&meter.soc[0], &meter_buffer[4], 4);							//Used Memcopy because the data truncation caused by the uint32_t -> float format conversion

				syslog(LOG_NOTICE, "Re-synchronized!. Counter[0] of this cycle is %d", meter.cont_amos[0]);


				for (j=1; j <= (C_SAMPLES_PER_CYCLE-1); j++){

					res = read(meter_pipe_fd, meter_buffer, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE); //20 is compliant with 5 values in float in 1 full cycle representation
					if (res <= -1){
						printf("PROBLEM DETECTED");
						printf("errno value: %d \n", errno);
						syslog(LOG_ERR, "Problem on reading samples from sampler handler fifo. Errno: %d", errno);

					}else if(res == 0){
						printf("PMU reading fifo closed, Closing APP.\n");
						syslog(LOG_ERR, "PMU to sampler handler fifo closed. Closing app. ERRNO = 0");

						exit(EXIT_SUCCESS);
					};
					 meter.cont_amos[j] 	= (uint16_t)meter_buffer[0] ; // counter from samples handler
					 meter.tensao_fa[j] 	= meter_buffer[1];
					 meter.tensao_fb[j] 	= meter_buffer[2];
					 meter.tensao_fc[j] 	= meter_buffer[3];
					 //meter.soc[j] =	(uint32_t)meter_buffer[4];
					 memcpy(&meter.soc[j], &meter_buffer[4], 4);							//Used Memcopy because the data truncation caused by the uint32_t -> float format conversion

					 //Space for currents - future implementation
				};
			};

			if (meter.cont_amos[0]==1){
				frac_sec = 0; //Fractional second part reseting
			}
			else
			{
				frac_sec++;
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 												DFT OPPERATION											//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Allowing dft calculus
		if(dft_calc_allow == false){
			dft_calc_allow = true;
		}else{
			freq_estimation_allow = true;
		};

		if (dft_calc_allow == true){
			phasor_Va = DFT_phasor(meter.tensao_fa, C_SAMPLES_PER_CYCLE, cycle_points_type);//C_SAMPLES_PER_PERIOD
			phasor_Vb = DFT_phasor(meter.tensao_fb, C_SAMPLES_PER_CYCLE, cycle_points_type);
			phasor_Vc = DFT_phasor(meter.tensao_fc, C_SAMPLES_PER_CYCLE, cycle_points_type);
			//printf("Phasor VB is %f + j%f  , mag = %f   , ang = %f grad\n",phasor_Vb.X_real, phasor_Vb.X_imag,  (float)sqrt((phasor_Vb.X_real*phasor_Vb.X_real) + (phasor_Vb.X_imag*phasor_Vb.X_imag) )    , (float)atan2(phasor_Vb.X_imag,phasor_Vb.X_real)*180/C_PI  );

		}else{ //Error Phasor
			phasor_Va.X_real = meter.tensao_fa[0]; 	phasor_Va.X_imag = meter.tensao_fa[0];
			phasor_Vb.X_real = meter.tensao_fb[0];	phasor_Vb.X_imag = meter.tensao_fb[0];
			phasor_Vc.X_real = meter.tensao_fc[0];  phasor_Vc.X_imag = meter.tensao_fc[0];
		};

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 						Adjusting the angle to compensate ADC and analog filters						//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		if ( _stimulus_mode == 0)
		{
			phasor_Va = adjust_phasor_angl(phasor_Va,0, _adjust_phasors_angle[0]); //C_ANGLE_COMPENSATION_A);  //Inputs: Phasor to adjust, Magntude adjust parcel, Angle adjust parcel
			phasor_Vb = adjust_phasor_angl(phasor_Vb,0, _adjust_phasors_angle[1]); //C_ANGLE_COMPENSATION_B);
			phasor_Vc = adjust_phasor_angl(phasor_Vc,0, _adjust_phasors_angle[2]); //C_ANGLE_COMPENSATION_C);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 										Sequence components opperation									//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		seq_components = Seq_compnts(phasor_Va, phasor_Vb, phasor_Vc);
		//printf("Positive Seq is %f + j%f \n",creal(seq_components.Pos_seq), cimag(seq_components.Pos_seq));     //printf("Negative Seq is %f + j%f \n",creal(seq_components.Neg_seq), cimag(seq_components.Neg_seq));


		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 									Frequency estimation opperation										//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		estimated_freq[1] = estimated_freq[0]; //shifts the last frequency value
		if (freq_estimation_allow == true){
			freq_angle_reference[2] = freq_angle_reference[1];
			freq_angle_reference[1] = freq_angle_reference[0];
			if (phasor_reference_for_freq_mode == FREQ_OBTAINED_FROM_PHASE_A_PHASOR)
			{
				freq_angle_reference[0] = atan2(phasor_Va.X_imag, phasor_Va.X_real);
			}
			else if ((phasor_reference_for_freq_mode == FREQ_OBTAINED_FROM_PHASE_B_PHASOR))
			{
				freq_angle_reference[0] = atan2(phasor_Vb.X_imag, phasor_Vb.X_real);
			}
			else //for positive seq
			{
				freq_angle_reference[0] = atan2(cimag(seq_components.Pos_seq), creal(seq_components.Pos_seq));
			}
			estimated_freq[0] = Freq_calc_phsr_ang(freq_angle_reference, _grid_nominal_freq); //The other necessary constants values are defined on defines.h file
			//printf("nom freq: %d , estimated freq: %f \n\r", _grid_nominal_freq, estimated_freq[0]);

		}else{
			if (phasor_reference_for_freq_mode == FREQ_OBTAINED_FROM_PHASE_A_PHASOR)
			{
				freq_angle_reference[0] = atan2(phasor_Va.X_imag, phasor_Va.X_real);
			}
			else //for positive seq
			{
				freq_angle_reference[0] = atan2(cimag(seq_components.Pos_seq), creal(seq_components.Pos_seq));
			}
			estimated_freq[0] = (float)_grid_nominal_freq;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 										ROCOF estimation calculus										//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		estimated_rocof = (estimated_freq[0] - estimated_freq[1])*(float)_grid_nominal_freq;


		if ( _stimulus_mode == 1)
		{
			//Save the phasors on output file for post processing
			write_as_medplot_output_file(meter.time[C_SAMPLES_PER_CYCLE-1], phasor_Va, phasor_Vb, phasor_Vc, estimated_freq[0], estimated_rocof);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 										Estimate and compensate Pgain									//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

        p_gain_compensation = P_Gain_dev_freq(estimated_freq[0], _grid_nominal_freq);

		phasor_Va.X_imag = cimag((phasor_Va.X_real+I*phasor_Va.X_imag)/p_gain_compensation);

		phasor_Va.X_real = creal((phasor_Va.X_real+I*phasor_Va.X_imag)/p_gain_compensation);

		phasor_Vb.X_imag = cimag((phasor_Vb.X_real+I*phasor_Vb.X_imag)/p_gain_compensation);
//			printf("PGAIN is %f + j%f \n",creal(p_gain_compensation), cimag(p_gain_compensation));
		phasor_Vb.X_real = creal((phasor_Vb.X_real+I*phasor_Vb.X_imag)/p_gain_compensation);

		phasor_Vc.X_imag = cimag((phasor_Vc.X_real+I*phasor_Vc.X_imag)/p_gain_compensation);
//			printf("PGAIN is %f + j%f \n",creal(p_gain_compensation), cimag(p_gain_compensation));
		phasor_Vc.X_real = creal((phasor_Vc.X_real+I*phasor_Vc.X_imag)/p_gain_compensation);

		if (number_of_phasors_to_send == 7)
		{
			seq_components.Pos_seq = (seq_components.Pos_seq/p_gain_compensation);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 										Protocol parameters handle - HEADER								//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		final_data_to_protocol.FRACSEC = frac_sec * frac_sec_adj; //frac_sec_array[frac_sec];//AS IMPLEMENTED ON THE REASON fracsec, used a lookup table. Was: (time_quality<<24)+frac_sec;// frac_sec; //time_quality<<24 +
		final_data_to_protocol.TIME_BASE = C_PMU_CFG_TIME_BASE; //3C
		final_data_to_protocol.SOC = meter.soc[0];

		if( ((final_data_to_protocol.SOC%60) == 0 && final_data_to_protocol.FRACSEC == 0)|| (first_iteration_flag ==1)){ //Send configuration and header frames to PDC every minute and at first iteration
			final_data_to_protocol.frame_type = 1; //Header
			///////////---------------------c37118_pipe mounting values -HEADER---------------------///////////
			c37118_pipe[0]= iter; //Counter value. // always  iteration number 0-255
			c37118_pipe[1] = final_data_to_protocol.frame_type; //always frame type
			c37118_pipe[2] = SOC_pointer_to_char_repres[3];
			c37118_pipe[3] = SOC_pointer_to_char_repres[2];
			c37118_pipe[4] = SOC_pointer_to_char_repres[1];
			c37118_pipe[5] = SOC_pointer_to_char_repres[0];
			c37118_pipe[6] = FRACSEC_pointer_to_char_repres[3];
			c37118_pipe[7] = FRACSEC_pointer_to_char_repres[2];
			c37118_pipe[8] = FRACSEC_pointer_to_char_repres[1];
			c37118_pipe[9] = FRACSEC_pointer_to_char_repres[0];

#ifndef _PMU_WORKS_WITHOUT_C37118
			if (_stimulus_mode == 0)
			{
				res_protocol = write(c37118_fifo_fd, c37118_pipe, fifo_size_in_bytes); //Sending
				iter++;
				if (res_protocol == -1) {//try write until fifo is not full
					//todo: Put a logguer logic here
					while(res_protocol < 0){ // Enquanto n�o consegue escrever
						res_protocol = write(c37118_fifo_fd, c37118_pipe, fifo_size_in_bytes);
						puts("PMU: FIFO PMU2c37 - write config frame, fifo full.");
						syslog(LOG_ERR, "Fifo to C37118 is full - write config frame");
					};
				};
			}
#endif
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			// 										Protocol parameters handle - CONFIG								//
			//																										//
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			final_data_to_protocol.frame_type = 3; //config CFG2
			//write_ieee37_118(final_data_to_protocol, uPMU_final_file, output_phasors_file_counter );
			///////////-----------------c37118_pipe mounting values -CONFIGURATION------------------///////////
			c37118_pipe[0]= iter; //Counter value.
			c37118_pipe[1] = final_data_to_protocol.frame_type; /// FRAME TYPE
			c37118_pipe[2] = SOC_pointer_to_char_repres[3];
			c37118_pipe[3] = SOC_pointer_to_char_repres[2];
			c37118_pipe[4] = SOC_pointer_to_char_repres[1];
			c37118_pipe[5] = SOC_pointer_to_char_repres[0];
			c37118_pipe[6] = FRACSEC_pointer_to_char_repres[3];
			c37118_pipe[7] = FRACSEC_pointer_to_char_repres[2];
			c37118_pipe[8] = FRACSEC_pointer_to_char_repres[1];
			c37118_pipe[9] = FRACSEC_pointer_to_char_repres[0];
			c37118_pipe[10] = TIME_BASE_pointers[3];//final_data_to_protocol.TIME_BASE>>24;
			c37118_pipe[11] = TIME_BASE_pointers[2]; //final_data_to_protocol.TIME_BASE>>16;
			c37118_pipe[12] = TIME_BASE_pointers[1];//final_data_to_protocol.TIME_BASE>>8;
			c37118_pipe[13] = TIME_BASE_pointers[0];//final_data_to_protocol.TIME_BASE;
#ifndef _PMU_WORKS_WITHOUT_C37118
			if (_stimulus_mode == 0)
			{
				res_protocol = write(c37118_fifo_fd, c37118_pipe, fifo_size_in_bytes);
				iter++;
				if (res_protocol == -1) {//try write until fifo is not full
					//todo: Put a logguer logic here
					while(res_protocol < 0){
						res_protocol = write(c37118_fifo_fd, c37118_pipe, fifo_size_in_bytes);
						puts("PMU: FIFO PMU2c37 - write config frame, fifo full.");
						syslog(LOG_ERR, "Fifo to C37118 is full - write config frame");
					}
				}
			}
#endif
			CLR_VAL(first_iteration_flag); //change_min_flag = 0;
			final_data_to_protocol.frame_type = 0; //Data
		};

		//DATA frame assertion
		final_data_to_protocol.STAT_DATA = 64;
		final_data_to_protocol.PhsA_real = phasor_Va.X_real;//*C_C37118_PHASES_GAIN;//(int16_t)(round(phasor_Va.X_real*C_C37118_PHASES_GAIN)); ///This is compensated on PHUNIT field on config frame
		final_data_to_protocol.PhsA_imag = phasor_Va.X_imag;//*C_C37118_PHASES_GAIN;//(int16_t)(round(phasor_Va.X_imag*C_C37118_PHASES_GAIN)); //This is compensated on PHUNIT field on config frame
		final_data_to_protocol.PhsB_real = phasor_Vb.X_real;//*C_C37118_PHASES_GAIN;//(int16_t)(roundf(phasor_Vb.X_real*C_C37118_PHASES_GAIN)); ///This is compensated on PHUNIT field on config frame
		final_data_to_protocol.PhsB_imag = phasor_Vb.X_imag;//*C_C37118_PHASES_GAIN;//(int16_t)(roundf(phasor_Vb.X_imag*C_C37118_PHASES_GAIN)); //This is compensated on PHUNIT field on config frame
		final_data_to_protocol.PhsC_real = phasor_Vc.X_real;//*C_C37118_PHASES_GAIN;//(int16_t)(roundf(phasor_Vc.X_real*C_C37118_PHASES_GAIN)); ///This is compensated on PHUNIT field on config frame
		final_data_to_protocol.PhsC_imag = phasor_Vc.X_imag;//*C_C37118_PHASES_GAIN;//(int16_t)(roundf(phasor_Vc.X_imag*C_C37118_PHASES_GAIN)); //This is compensated on PHUNIT field on config frame

		if (number_of_phasors_to_send == 7)
		{
			final_data_to_protocol.Pos_seq_real = creal(seq_components.Pos_seq);
			final_data_to_protocol.Pos_seq_imag = cimag(seq_components.Pos_seq);
		}

		final_data_to_protocol.DFREQ = estimated_rocof;  //*100); //*100 to be compliant with the STD c37.118-2 2011
		final_data_to_protocol.FREQ = estimated_freq[0];// - (float)sys_frequency) * 1000) ; //ADJUST THAT INTO A mHz scalling deviation in data frame (62.5HZ = +2500mHz = 0x09C4 deviation)

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 										Protocol parameters handle - DATA								//
		//																										//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		if (number_of_phasors_to_send == 3) //Phasor VA, VB, VC
		{
			c37118_pipe[0]= iter; //Counter value.
			c37118_pipe[1] = final_data_to_protocol.frame_type;
			c37118_pipe[2] = SOC_pointer_to_char_repres[3];
			c37118_pipe[3] = SOC_pointer_to_char_repres[2];
			c37118_pipe[4] = SOC_pointer_to_char_repres[1];
			c37118_pipe[5] = SOC_pointer_to_char_repres[0];

			c37118_pipe[6] = FRACSEC_pointer_to_char_repres[3];
			c37118_pipe[7] = FRACSEC_pointer_to_char_repres[2];
			c37118_pipe[8] = FRACSEC_pointer_to_char_repres[1];
			c37118_pipe[9] = FRACSEC_pointer_to_char_repres[0];

			c37118_pipe[10] = STAT_DATA_pointers[1];
			c37118_pipe[11] = STAT_DATA_pointers[0];
			//Phase A
			c37118_pipe[12] = PhsA_real_pointers[3];//final_data_to_protocol.PhsA_real >>8; //PHASE A
			c37118_pipe[13] = PhsA_real_pointers[2];//final_data_to_protocol.PhsA_real; //PHASE A
			c37118_pipe[14] = PhsA_real_pointers[1];
			c37118_pipe[15] = PhsA_real_pointers[0];

			c37118_pipe[16] = PhsA_imag_pointers[3];//final_data_to_protocol.PhsA_imag >>8; //PHASE A
			c37118_pipe[17] = PhsA_imag_pointers[2];//final_data_to_protocol.PhsA_imag; //PHASE A
			c37118_pipe[18] = PhsA_imag_pointers[1];
			c37118_pipe[19] = PhsA_imag_pointers[0];
			//Phase B
			c37118_pipe[20] = PhsB_real_pointers[3];//final_data_to_protocol.PhsB_real >>8; //PHASE B
			c37118_pipe[21] = PhsB_real_pointers[2];//final_data_to_protocol.PhsB_real; //PHASE B
			c37118_pipe[22] = PhsB_real_pointers[1];
			c37118_pipe[23] = PhsB_real_pointers[0];

			c37118_pipe[24] = PhsB_imag_pointers[3];//final_data_to_protocol.PhsB_imag >>8; //PHASE B
			c37118_pipe[25] = PhsB_imag_pointers[2];//final_data_to_protocol.PhsB_imag; //PHASE B
			c37118_pipe[26] = PhsB_imag_pointers[1];
			c37118_pipe[27] = PhsB_imag_pointers[0];
			//Phase C
			c37118_pipe[28] = PhsC_real_pointers[3];//final_data_to_protocol.PhsC_real >>8; //PHASE C
			c37118_pipe[29] = PhsC_real_pointers[2];//final_data_to_protocol.PhsC_real; //PHASE C
			c37118_pipe[30] = PhsC_real_pointers[1];
			c37118_pipe[31] = PhsC_real_pointers[0];

			c37118_pipe[32] = PhsC_imag_pointers[3];//final_data_to_protocol.PhsC_imag >>8; //PHASE C
			c37118_pipe[33] = PhsC_imag_pointers[2];//final_data_to_protocol.PhsC_imag; //PHASE C
			c37118_pipe[34] = PhsC_imag_pointers[1];
			c37118_pipe[35] = PhsC_imag_pointers[0];

			//Freq points to final_data_to_protocol.FREQ variable
			c37118_pipe[36] = FREQ_pointer_to_char_representation[3]; //point to (final_data_to_protocol.FREQ)>>24) //Frequency
			c37118_pipe[37] = FREQ_pointer_to_char_representation[2];//(char)((uint32_t)(final_data_to_protocol.FREQ) >>16); //Frequency
			c37118_pipe[38] = FREQ_pointer_to_char_representation[1];//(char)((uint32_t)(final_data_to_protocol.FREQ) >>8); //Frequency
			c37118_pipe[39] = FREQ_pointer_to_char_representation[0];//(char)((uint32_t)(final_data_to_protocol.FREQ)); //Frequency
			//ROCOF to final_data_to_protocol.DFREQ variable
			c37118_pipe[40] = DFREQ_pointer_to_char_representation[3];//(int32_t)(final_data_to_protocol.DFREQ) >>24; //ROCOF
			c37118_pipe[41] = DFREQ_pointer_to_char_representation[2];//(int32_t)(final_data_to_protocol.DFREQ) >>16; //ROCOF
			c37118_pipe[42] = DFREQ_pointer_to_char_representation[1];//(int32_t)(final_data_to_protocol.DFREQ) >>8; //ROCOF
			c37118_pipe[43] = DFREQ_pointer_to_char_representation[0];//(int32_t)(final_data_to_protocol.DFREQ); //ROCOF

		}
		else if (number_of_phasors_to_send == 6) //Phasors VA, IA, VB, IB, VC, IC
		{
			c37118_pipe[0]= iter; //Counter value.
			c37118_pipe[1] = final_data_to_protocol.frame_type;
			c37118_pipe[2] = SOC_pointer_to_char_repres[3];
			c37118_pipe[3] = SOC_pointer_to_char_repres[2];
			c37118_pipe[4] = SOC_pointer_to_char_repres[1];
			c37118_pipe[5] = SOC_pointer_to_char_repres[0];
			c37118_pipe[6] = FRACSEC_pointer_to_char_repres[3];
			c37118_pipe[7] = FRACSEC_pointer_to_char_repres[2];
			c37118_pipe[8] = FRACSEC_pointer_to_char_repres[1];
			c37118_pipe[9] = FRACSEC_pointer_to_char_repres[0];
			c37118_pipe[10] = STAT_DATA_pointers[1];
			c37118_pipe[11] = STAT_DATA_pointers[0];
			//Phase A
			c37118_pipe[12] = PhsA_real_pointers[3];//final_data_to_protocol.PhsA_real >>8; //PHASE A
			c37118_pipe[13] = PhsA_real_pointers[2];//final_data_to_protocol.PhsA_real; //PHASE A
			c37118_pipe[14] = PhsA_real_pointers[1];
			c37118_pipe[15] = PhsA_real_pointers[0];

			c37118_pipe[16] = PhsA_imag_pointers[3];//final_data_to_protocol.PhsA_imag >>8; //PHASE A
			c37118_pipe[17] = PhsA_imag_pointers[2];//final_data_to_protocol.PhsA_imag; //PHASE A
			c37118_pipe[18] = PhsA_imag_pointers[1];
			c37118_pipe[19] = PhsA_imag_pointers[0];
			//Phase B
			c37118_pipe[20] = PhsB_real_pointers[3];//final_data_to_protocol.PhsB_real >>8; //PHASE B
			c37118_pipe[21] = PhsB_real_pointers[2];//final_data_to_protocol.PhsB_real; //PHASE B
			c37118_pipe[22] = PhsB_real_pointers[1];
			c37118_pipe[23] = PhsB_real_pointers[0];

			c37118_pipe[24] = PhsB_imag_pointers[3];//final_data_to_protocol.PhsB_imag >>8; //PHASE B
			c37118_pipe[25] = PhsB_imag_pointers[2];//final_data_to_protocol.PhsB_imag; //PHASE B
			c37118_pipe[26] = PhsB_imag_pointers[1];
			c37118_pipe[27] = PhsB_imag_pointers[0];
			//Phase C
			c37118_pipe[28] = PhsC_real_pointers[3];//final_data_to_protocol.PhsC_real >>8; //PHASE C
			c37118_pipe[29] = PhsC_real_pointers[2];//final_data_to_protocol.PhsC_real; //PHASE C
			c37118_pipe[30] = PhsC_real_pointers[1];
			c37118_pipe[31] = PhsC_real_pointers[0];

			c37118_pipe[32] = PhsC_imag_pointers[3];//final_data_to_protocol.PhsC_imag >>8; //PHASE C
			c37118_pipe[33] = PhsC_imag_pointers[2];//final_data_to_protocol.PhsC_imag; //PHASE C
			c37118_pipe[34] = PhsC_imag_pointers[1];
			c37118_pipe[35] = PhsC_imag_pointers[0];

			//Freq points to final_data_to_protocol.FREQ variable
			c37118_pipe[36] = FREQ_pointer_to_char_representation[3]; //point to (final_data_to_protocol.FREQ)>>24) //Frequency
			c37118_pipe[37] = FREQ_pointer_to_char_representation[2];//(char)((uint32_t)(final_data_to_protocol.FREQ) >>16); //Frequency
			c37118_pipe[38] = FREQ_pointer_to_char_representation[1];//(char)((uint32_t)(final_data_to_protocol.FREQ) >>8); //Frequency
			c37118_pipe[39] = FREQ_pointer_to_char_representation[0];//(char)((uint32_t)(final_data_to_protocol.FREQ)); //Frequency
			//ROCOF to final_data_to_protocol.DFREQ variable
			c37118_pipe[40] = DFREQ_pointer_to_char_representation[3];//(int32_t)(final_data_to_protocol.DFREQ) >>24; //ROCOF
			c37118_pipe[41] = DFREQ_pointer_to_char_representation[2];//(int32_t)(final_data_to_protocol.DFREQ) >>16; //ROCOF
			c37118_pipe[42] = DFREQ_pointer_to_char_representation[1];//(int32_t)(final_data_to_protocol.DFREQ) >>8; //ROCOF
			c37118_pipe[43] = DFREQ_pointer_to_char_representation[0];//(int32_t)(final_data_to_protocol.DFREQ); //ROCOF

		}
		else //7 Phasors VA, IA, VB, IB, VC, IC, PosSeqV
		{
			c37118_pipe[0]= iter; //Counter value.
			c37118_pipe[1] = final_data_to_protocol.frame_type;
			c37118_pipe[2] = SOC_pointer_to_char_repres[3];
			c37118_pipe[3] = SOC_pointer_to_char_repres[2];
			c37118_pipe[4] = SOC_pointer_to_char_repres[1];
			c37118_pipe[5] = SOC_pointer_to_char_repres[0];
			c37118_pipe[6] = FRACSEC_pointer_to_char_repres[3];
			c37118_pipe[7] = FRACSEC_pointer_to_char_repres[2];
			c37118_pipe[8] = FRACSEC_pointer_to_char_repres[1];
			c37118_pipe[9] = FRACSEC_pointer_to_char_repres[0];
			c37118_pipe[10] = STAT_DATA_pointers[1];
			c37118_pipe[11] = STAT_DATA_pointers[0];
			//Phase A
			c37118_pipe[12] = PhsA_real_pointers[3];//final_data_to_protocol.PhsA_real >>8; //PHASE A
			c37118_pipe[13] = PhsA_real_pointers[2];//final_data_to_protocol.PhsA_real; //PHASE A
			c37118_pipe[14] = PhsA_real_pointers[1];
			c37118_pipe[15] = PhsA_real_pointers[0];

			c37118_pipe[16] = PhsA_imag_pointers[3];//final_data_to_protocol.PhsA_imag >>8; //PHASE A
			c37118_pipe[17] = PhsA_imag_pointers[2];//final_data_to_protocol.PhsA_imag; //PHASE A
			c37118_pipe[18] = PhsA_imag_pointers[1];
			c37118_pipe[19] = PhsA_imag_pointers[0];
			//Phase B
			c37118_pipe[20] = PhsB_real_pointers[3];//final_data_to_protocol.PhsB_real >>8; //PHASE B
			c37118_pipe[21] = PhsB_real_pointers[2];//final_data_to_protocol.PhsB_real; //PHASE B
			c37118_pipe[22] = PhsB_real_pointers[1];
			c37118_pipe[23] = PhsB_real_pointers[0];

			c37118_pipe[24] = PhsB_imag_pointers[3];//final_data_to_protocol.PhsB_imag >>8; //PHASE B
			c37118_pipe[25] = PhsB_imag_pointers[2];//final_data_to_protocol.PhsB_imag; //PHASE B
			c37118_pipe[26] = PhsB_imag_pointers[1];
			c37118_pipe[27] = PhsB_imag_pointers[0];
			//Phase C
			c37118_pipe[28] = PhsC_real_pointers[3];//final_data_to_protocol.PhsC_real >>8; //PHASE C
			c37118_pipe[29] = PhsC_real_pointers[2];//final_data_to_protocol.PhsC_real; //PHASE C
			c37118_pipe[30] = PhsC_real_pointers[1];
			c37118_pipe[31] = PhsC_real_pointers[0];

			c37118_pipe[32] = PhsC_imag_pointers[3];//final_data_to_protocol.PhsC_imag >>8; //PHASE C
			c37118_pipe[33] = PhsC_imag_pointers[2];//final_data_to_protocol.PhsC_imag; //PHASE C
			c37118_pipe[34] = PhsC_imag_pointers[1];
			c37118_pipe[35] = PhsC_imag_pointers[0];
			//Pos seq.
			c37118_pipe[36] = PhsPpos_real_pointers[3]; //Pos seq
			c37118_pipe[37] = PhsPpos_real_pointers[2]; //Pos seq
			c37118_pipe[38] = PhsPpos_real_pointers[1]; //Pos seq
			c37118_pipe[39] = PhsPpos_real_pointers[0]; //Pos seq

			c37118_pipe[40] = PhsPpos_imag_pointers[3];
			c37118_pipe[41] = PhsPpos_imag_pointers[2];
			c37118_pipe[42] = PhsPpos_imag_pointers[1];
			c37118_pipe[43] = PhsPpos_imag_pointers[0];

			//Freq points to final_data_to_protocol.FREQ variable
			c37118_pipe[44] = FREQ_pointer_to_char_representation[3]; //point to (final_data_to_protocol.FREQ)>>24) //Frequency
			c37118_pipe[45] = FREQ_pointer_to_char_representation[2];//(char)((uint32_t)(final_data_to_protocol.FREQ) >>16); //Frequency
			c37118_pipe[46] = FREQ_pointer_to_char_representation[1];//(char)((uint32_t)(final_data_to_protocol.FREQ) >>8); //Frequency
			c37118_pipe[47] = FREQ_pointer_to_char_representation[0];//(char)((uint32_t)(final_data_to_protocol.FREQ)); //Frequency
			//ROCOF to final_data_to_protocol.DFREQ variable
			c37118_pipe[48] = DFREQ_pointer_to_char_representation[3];//(int32_t)(final_data_to_protocol.DFREQ) >>24; //ROCOF
			c37118_pipe[49] = DFREQ_pointer_to_char_representation[2];//(int32_t)(final_data_to_protocol.DFREQ) >>16; //ROCOF
			c37118_pipe[50] = DFREQ_pointer_to_char_representation[1];//(int32_t)(final_data_to_protocol.DFREQ) >>8; //ROCOF
			c37118_pipe[51] = DFREQ_pointer_to_char_representation[0];//(int32_t)(final_data_to_protocol.DFREQ); //ROCOF
		}
//#endif


//Writting the processed information to protocol program handler
//write_ieee37_118(final_data_to_protocol, uPMU_final_file, output_phasors_file_counter );
#ifndef _PMU_WORKS_WITHOUT_C37118
		if (_stimulus_mode == 0)
		{
			res_protocol = write(c37118_fifo_fd, c37118_pipe, fifo_size_in_bytes);
			iter++;
			if (res_protocol == -1)
			{//try write until fifo is not full
				//todo: Put a logguer logic here
				while(res_protocol < 0){
					res_protocol = write(c37118_fifo_fd, c37118_pipe, fifo_size_in_bytes);
					puts("PMU: FIFO PMU2c37 - write config frame, fifo full.");
					syslog(LOG_ERR, "Fifo to C37118 is full");
				}
			}
		}
#endif
	} //while(1)
} // int main(void)
