/*
 * read_stimulus_file.c
 *
 *  Created on: 21 de abr de 2018
 *      Author: Maique Garcia
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "read_stimulus_file.h"

struct meter_values read_stimulus_file( int header_lines, long int file_l_pointer, uint8_t reserved){

	FILE *fp;	//File reading var
	struct meter_values meter;
	char buf[255];//File reading var
	//long int file_l_pointer = 0;//File reading pointer
	uint8_t i = 0; //Iterations to mount the read vector.


	//Oppening file
	fp = fopen(C_SER_PMU_STIM_FILE0, "r"); //open in read mode
	//Reading values from the input file
	if (fp == NULL)
	{
		puts("Error on opening FILE");
	}
	//Moving to desired position
	fseek( fp, file_l_pointer, SEEK_SET );
	//Reading header unused lines
	if (header_lines > 0)
	{
		while(fgetc(fp) != '\n')
		{
			puts("excluded 1 char");
		}
	}
	//Reading Cycle samples amount
	while (i < C_SAMPLES_PER_CYCLE)
	{
		//Scanning the SOC
		fscanf(fp,"%f",&meter.SOC[i]); //This function scan until the new value (excluding the nulls, spaces and tab characters)
			//printf("SOC get is : %f ",meter.SOC[i]);
		//Scanning the VA
		fscanf(fp,"%f",&meter.Va_mod[i]);
		meter.Va_mod[i] = meter.Va_mod[i]*(pow(10,C_VALUE_SCALE)); //scalling factor
			//printf("VA get is : %f ",meter.Va_mod[i]);
		//Scanning the VB
		fscanf(fp,"%f",&meter.Vb_mod[i]);
		meter.Vb_mod[i] = meter.Vb_mod[i]*(pow(10,C_VALUE_SCALE)); //scalling factor
			//printf("VB get is : %f ",meter.Vb_mod[i]);
		//Scanning the VC
		fscanf(fp,"%f",&meter.Vc_mod[i]);
		meter.Vc_mod[i] =meter.Vc_mod[i]*(pow(10,C_VALUE_SCALE)); //scalling factor
			//printf("VC get is : %f \n",meter.Vc_mod[i]);
		i++;
	}

	meter.file_pointer = ftell(fp); //reading the last file position
	return meter;
	fclose(fp);//Closing file
};
